const API_URL = "https://jsonplaceholder.typicode.com/todos/"


async function loadTasks() {
    const response = await fetch(API_URL)
    const json = await response.json()
    return json
}

apiUrlElement = document.getElementById("api-url")
apiUrlElement.innerHTML = API_URL
apiUrlElement.href = API_URL

loadTasks()
    .then(json => {
        tasksCountElement = document.getElementById("tasks-count")
        tasksCountElement.innerHTML = json.length

        completedTasksCountElement = document.getElementById("completed-tasks-count")
        completedTasksCountElement.innerHTML = json.filter(task => task.completed).length

        notCompletedTasksCountElement = document.getElementById("not-completed-tasks-count")
        notCompletedTasksCountElement.innerHTML = json.filter(task => !task.completed).length
        
        firstTenTaksElement = document.getElementById("first-ten-tasks")
        firstTenTaksElement.innerHTML = json.slice(0, 10).map(task => `<li>${task.title}</li>`).join("")

        rawDataElement = document.getElementById("raw-data")
        rawDataElement.innerHTML = JSON.stringify(json, null, 4)
    })